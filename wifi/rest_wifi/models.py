from django.db import models


class WifiNetwork(models.Model):
    """
    Wifi network instance representation
    """

    id = models.CharField(primary_key=True, max_length=200, blank=False)
    auth = models.CharField(max_length=12)
    avg_throughput = models.FloatField('average throughput', default=0)

    def __str__(self):
        return str(self.id)


class Device(models.Model):
    """
    Device connected to Wifi network representation
    """

    wifi_network = models.ForeignKey(WifiNetwork, related_name='devices', on_delete=models.CASCADE)
    id = models.CharField(primary_key=True, max_length=200)
    throughput = models.FloatField("device throughput", default=0)
    auth = models.CharField(max_length=12)

    def __str__(self):
        return str(self.id)
