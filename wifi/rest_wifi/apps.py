from django.apps import AppConfig


class RestWifiConfig(AppConfig):
    name = 'rest_wifi'
