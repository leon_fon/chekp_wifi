from rest_framework import serializers
from rest_wifi.models import WifiNetwork, Device


def validate_auth(auth):
    """
    Custom validator for network and device authentication
    :param auth: authentication field from serializer
    :return: raise ValidationError in case that the field don't match
    """
    if auth.lower() not in ['wpa', 'public']:
        raise serializers.ValidationError("The authentication must be WPA or Public only")


class DevicesField(serializers.RelatedField):
    """
    Device field representation for WifiNetwork model
    """
    def to_representation(self, device):
        return {'id': device.id, 'throughput': device.throughput}


class WifiNetworkSerializer(serializers.Serializer):
    """
    Serialization and validation between WifiNetwork model to API view
    """
    id = serializers.CharField(max_length=200)
    auth = serializers.CharField(max_length=12, validators=[validate_auth])
    avg_throughput = serializers.FloatField(default=0)
    devices = DevicesField(many=True, read_only=True)

    def create(self, validated_data):
        """
        Create and return a new `WifiNetwork` instance, given the validated data.
        """
        return WifiNetwork.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `WifiNetwork` instance, given the validated data.
        """
        instance.auth = validated_data.get('auth', instance.auth)
        instance.avg_throughput = validated_data.get('avg_throughput', instance.avg_throughput)
        instance.save()
        return instance


class DeviceSerializer(serializers.Serializer):
    """
    Serialization and validation between Device model to API view
    """
    network_id = serializers.CharField(max_length=200, source='wifi_network')
    device_id = serializers.CharField(max_length=200, source='id')
    auth = serializers.CharField(max_length=12, allow_blank=True, validators=[validate_auth])
    throughput = serializers.FloatField(default=0)

    def create(self, validated_data):
        """
        Create and return a new `Device` instance, given the validated data and perform additional validations.
        """
        validated_data['auth'] = validated_data['auth'].lower()
        network_id = validated_data['wifi_network']

        try:
            wifi_network = WifiNetwork.objects.get(pk=network_id)
        except WifiNetwork.DoesNotExist:
            wifi_network = WifiNetwork(id=network_id, auth=validated_data['auth'])
            wifi_network.save()

        if validated_data['auth'].lower() != wifi_network.auth.lower():
            raise serializers.ValidationError("Device authentication should be the same as network authentication ({})"
                                              .format(wifi_network.auth))

        validated_data['wifi_network'] = wifi_network
        return Device.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Device` instance, given the validated data.
        """
        instance.auth = instance.auth
        instance.throughput = validated_data.get('throughput', instance.throughput)
        instance.save()
        return instance
