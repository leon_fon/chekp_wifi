from rest_wifi.models import WifiNetwork, Device
from rest_wifi.serializers import WifiNetworkSerializer, DeviceSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class WifiNetworkListAPI(APIView):
    """
    Get and post network
    """
    def get(self, request):
        """
        Get list of all existing networks
        """
        try:
            networks = WifiNetwork.objects.all()
        except WifiNetwork.DoesNotExist:
            msg = "There is no Wifi networks yet"
            return Response([msg], status=status.HTTP_404_NOT_FOUND)
        serializer = WifiNetworkSerializer(networks, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        Create new network
        """
        serializer = WifiNetworkSerializer(data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WifiNetworkDetailAPI(APIView):
    """
    Get and change wifi network by id
    """
    def get(self, request, pk):
        """
        Get network by id (primary key)
        """
        try:
            network = WifiNetwork.objects.get(pk=pk)
        except WifiNetwork.DoesNotExist:
            msg = "Network with ID {} not exist".format(pk)
            return Response([msg], status=status.HTTP_404_NOT_FOUND)
        serializer = WifiNetworkSerializer(network)
        return Response(serializer.data)

    def put(self, request, pk):
        """
        Update network by id (primary key)
        """
        try:
            network = WifiNetwork.objects.get(pk=pk)
        except WifiNetwork.DoesNotExist as e:
            msg = "Network with ID {} not exist".format(pk)
            return Response([msg], status=status.HTTP_404_NOT_FOUND)

        serializer = WifiNetworkSerializer(network, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        """
        delete network by id (primary key)
        """
        try:
            network = WifiNetwork.objects.get(pk=pk)
        except WifiNetwork.DoesNotExist:
            msg = "Network with ID {} not exist".format(pk)
            return Response([msg], status=status.HTTP_404_NOT_FOUND)
        network.delete()
        return Response("OK")


class DeviceConnectAPI(APIView):
    """
    Create a device and connect it to network
    """
    def post(self, request, format=None):
        try:
            Device.objects.get(pk=request.data['device_id'])
        except Device.DoesNotExist:
            pass
        else:
            return Response(['device with this ID already connected'], status.HTTP_400_BAD_REQUEST)

        serializer = DeviceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeviceReportAPI(APIView):
    """
    Report device throughput and update average throughput of network
    """
    def put(self, request, format=None):
        try:
            network = WifiNetwork.objects.get(pk=request.data['network_id'])
        except WifiNetwork.DoesNotExist:
            msg = "Device with ID {} not exist".format(request.data['network_id'])
            return Response([msg], status=status.HTTP_404_NOT_FOUND)
        devices = network.devices.all()

        if request.data['device_id'] not in [device.id for device in devices]:

            msg = "Device with ID {} not founded in network with ID {}".format(
                request.data['device_id'], request.data['network_id'])
            return Response([msg], status.HTTP_404_NOT_FOUND)

        device = [device for device in devices if device.id == request.data['device_id']][0]

        serializer = DeviceSerializer(device, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()

            devices = network.devices.exclude(throughput=0)
            network.avg_throughput = sum([device.throughput for device in devices]) / len(devices)
            network.save()

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
