from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^network/$', views.WifiNetworkListAPI.as_view(), name='network_list'),
    url(r'^network/connect/$', views.DeviceConnectAPI.as_view(), name='connect_device'),
    url(r'^network/report/$', views.DeviceReportAPI.as_view(), name='report_device_throughput'),
    url(r'^network/(?P<pk>.+)/$', views.WifiNetworkDetailAPI.as_view(), name='network_detail'),

]

