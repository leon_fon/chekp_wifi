angular.module("basiClient").service('WifiAPI', ['$http', function ($http) {
    let self = this;
    let url = {
        network: "/api/network/",
        connect: "/api/network/connect/",
        report: "/api/network/report/"
    };

    self.getNetworksList = function () {
        let request = {
            url: url.network,
            method: 'GET',
        };
        return $http(request)
    };

    self.createNetwork = function (data) {
        let request = {
            url: url.network,
            method: 'POST',
            data: data
        };
        return $http(request)
    };

    self.deleteNetwork = function (id) {
        let request = {
            url: url.network + id,
            method: 'DELETE'
        };
        return $http(request)
    };

    self.connectDevice = function (data) {
      let request = {
          url: url.connect,
          method: 'POST',
          data: data
      };
      return $http(request)
    };

    self.reportDevice = function (data) {
      let request = {
          url: url.report,
          method: 'PUT',
          data: data
      };
      return $http(request)
    };


}]);