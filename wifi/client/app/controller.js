angular.module("basiClient").controller("mainCtrl", function ($scope, WifiAPI) {

    let self = this;
    self.networks = [];
    self.network = {};
    self.device = {};
    self.error = '';
    (self.getNetworksList = function () {
        WifiAPI.getNetworksList().then(function(res){
        self.networks = res.data;
        })
    })();

    self.createNetwork = function (data) {
        WifiAPI.createNetwork(data).then(function (res) {
            self.networks.push(res.data);
            self.network = {};
            self.error = '';
        }, function (error) {
            self.error = error.data;
        });
    };

    self.deleteNetwork = function (id) {
        WifiAPI.deleteNetwork(id).then(function (res) {
            self.getNetworksList();
        })
    };

    self.connectDevice = function (data) {
        WifiAPI.connectDevice(data).then(function (res) {
            self.getNetworksList();
            self.error = '';
        }, function (error) {
            self.error = error.data;
        });
    };

    self.reportDevice = function (data) {
        WifiAPI.reportDevice(data).then(function (res) {
            self.getNetworksList();
            self.device = {};
            self.error = '';
        }, function (error) {
            self.error = error.data;
        });
    };


});