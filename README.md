# Check Point home assignment
## WiFi network analyzer


### Prerequisites

Python 3.6.x (with pip)

Bower (for client)

### Installing

Install requirements from requirements.txt file

```bash
cd <project-root>
pip install -r requirements.txt
```

Install client requirements

```bash
cd <project-root>/wifi/client
bower install

```

## Run the application

First perform database migration
```bash
cd <project-root>/wifi
python manage.py migrate
```

Then run the server
```bash
python manage.py runserver
```

After all enter [http://localhost:8000] in the address bar of your favorite browser.

Basic UI client should appear.

## Author

Leonid Fonaryov Rubin
